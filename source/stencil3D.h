#ifndef EXERCISE_07_STENCIL3D_H
#define EXERCISE_07_STENCIL3D_H

#include <iostream>
#include <vector>
#include <cmath>


class Stencil3D {
    unsigned int cube_size;

    double north_boundary;
    double east_boundary;
    double south_boundary;
    double west_boundary;
    double upper_boundary;
    double lower_boundary;

    using Cube = std::vector<std::vector<std::vector<double>>>;
    Cube cells;

public:
    Stencil3D(unsigned int size, double north, double east, double south, double west, double upper, double lower) :
            cube_size(size + 2), north_boundary(north), east_boundary(east), south_boundary(south), west_boundary(west), upper_boundary(upper), lower_boundary(lower) {    // +2 for boundaries; operating area is then from 1 to N in both dimensions

        cells = Cube(cube_size, std::vector<std::vector<double>>(cube_size, std::vector<double>(cube_size)));

        // setup boundaries
        set_boundaries();

        // std::cout << "Created a Stencil 3D of size " << cube_size << "x" << cube_size << "x" << cube_size << "(operating size: " << cube_size-2 << "x" << cube_size-2 << "x" << cube_size-2 << ")\n" << std::endl;
    }

    ~Stencil3D() = default;

    void set_boundaries() {
        // set back pane (north) and front pane (south)
        for(unsigned j = 1; j < cube_size-1; j++) {
            for(unsigned k = 1; k < cube_size-1; k++) {
                cells[0][j][k] = north_boundary;            // back pane (north)
                cells[cube_size-1][j][k] = south_boundary;  // front pane (south)
            }
        }

        // set left pane (west) and right pane (east)
        for(unsigned i = 1; i < cube_size-1; i++) {
            for(unsigned k = 1; k < cube_size-1; k++) {
                cells[i][0][k] = west_boundary;             // left pane (west)
                cells[i][cube_size-1][k] = east_boundary;   // right pane (east)
            }
        }

        // set lower and upper pane
        for(unsigned i = 1; i < cube_size-1; i++) {
            for(unsigned j = 1; j < cube_size-1; j++) {
                cells[i][j][0] = lower_boundary;            // lower pane
                cells[i][j][cube_size-1] = upper_boundary;  // upper pane
            }
        }
    }

    double jacobi_iteration_3d(const std::vector<std::vector<std::vector<double>>> &in, std::vector<std::vector<std::vector<double>>> &out, size_t size) {
        double sum_changes = 0;

        #pragma omp parallel for reduction(+:sum_changes)
        for(unsigned i = 1; i < size - 1; i++) {
            for(unsigned j = 1; j < size - 1; j++) {
                for(unsigned k = 1; k < size - 1; k++) {
                    out[i][j][k] = (  in[i    ][j    ][k    ] +       // current position
                                      in[i - 1][j    ][k    ] +       // position northern to current position
                                      in[i + 1][j    ][k    ] +       // position southern to current position
                                      in[i    ][j - 1][k    ] +       // position western to current position
                                      in[i    ][j + 1][k    ] +       // position eastern to current position
                                      in[i    ][j    ][k - 1] +       // position in plane below current position
                                      in[i    ][j    ][k + 1]) / 7;   // position in plane above current position

                    sum_changes += std::abs(out[i][j][k] - in[i][j][k]);
                }
            }
        }

        return sum_changes;
    }

    void calculate_stencil(double epsilon_value) {
        double sum_changes = epsilon_value;

        int iterations = 0;
        Cube temp = cells;
        while(sum_changes >= epsilon_value) {   // simulate as long as sum changes is >= epsilon
            jacobi_iteration_3d(temp, cells, cube_size);
            sum_changes=jacobi_iteration_3d(cells, temp, cube_size);
            iterations += 2;
        }

        // std::cout << "Simulation took " << iterations << " iteration(s)." << std::endl;
    }

    const Cube &getCells() const {
        return cells;
    }

    /*
     * just able to print the panes of the cube to check if boundaries are fine.
     * Printing whole cube is currently not possible.
     */
    void print_cube() {
        // print back pane (north)
        std::cout << "Back pane (north):\n";
        for(unsigned j = 0; j < cube_size; j++) {
            for(unsigned k = 0; k < cube_size; k++) {
                std::cout << cells[0][j][k] << " ";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;

        // print right pane (east)
        std::cout << "Right pane (east):\n";
        for(unsigned i = 0; i < cube_size; i++) {
            for(unsigned k = 0; k < cube_size; k++) {
                std::cout << cells[i][cube_size-1][k] << " ";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;

        // print front pane (south)
        std::cout << "Front pane (south):\n";
        for(unsigned j = 0; j < cube_size; j++) {
            for(unsigned k = 0; k < cube_size; k++) {
                std::cout << cells[cube_size-1][j][k] << " ";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;

        // print left pane (west)
        std::cout << "Left pane (west):\n";
        for(unsigned i = 0; i < cube_size; i++) {
            for(unsigned k = 0; k < cube_size; k++) {
                std::cout << cells[i][0][k] << " ";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;

        // print upper pane
        std::cout << "Upper pane:\n";
        for(unsigned i = 0; i < cube_size; i++) {
            for(unsigned j = 0; j < cube_size; j++) {
                std::cout << cells[i][j][cube_size-1] << " ";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;

        // print lower pane
        std::cout << "Lower pane:\n";
        for(unsigned i = 0; i < cube_size; i++) {
            for(unsigned j = 0; j < cube_size; j++) {
                std::cout << cells[i][j][0] << " ";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;
    }
};

#endif //EXERCISE_07_STENCIL3D_H
