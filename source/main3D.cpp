#include <iostream>
#include "stencil3D.h"
#include "util.h"
#include "chrono_timer.h"


int main(int argc, char** argv) {
    // default values
    unsigned int size = 100;
    double north_boundary = 1.0;
    double east_boundary = 0.5;
    double south_boundary = 0.0;
    double west_boundary = -0.5;
    double upper_boundary = 0.0;
    double lower_boundary = 0.0;
    double epsilon_value = 100.0;
    
    if(argc == 9) {
        size = (unsigned) std::stoi(argv[1]);
        north_boundary = std::stod(argv[2]);
        east_boundary = std::stod(argv[3]);
        south_boundary = std::stod(argv[4]);
        west_boundary = std::stod(argv[5]);
        upper_boundary = std::stod(argv[6]);
        lower_boundary = std::stod(argv[7]);
        epsilon_value = std::stod(argv[8]);
    }
    else {
        std::cout << "No input! Default values will be used! "
                     "Usage: ./stencil3D "
                     "<size> "
                     "<north-boundary> "
                     "<east-boundary> "
                     "<south-boundary> "
                     "<west-boundary> "
                     "<upper-boundary> "
                     "<lower-boundary> "
                     "<epsilon-value>" << std::endl;
    }

    /*
    std::cout << "size: " << size <<
              "\nnorth boundary: " << north_boundary <<
              "\neast boundary: " << east_boundary <<
              "\nsouth boundary: " << south_boundary <<
              "\nwest boundary: " << west_boundary <<
              "\nupper boundary: " << upper_boundary <<
              "\nlower boundary: " << lower_boundary << 
              "\nepsilon value: " << epsilon_value << std::endl;
    */
    
    int runs = 7;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        Stencil3D stencil3D(size, north_boundary, east_boundary, south_boundary, west_boundary, upper_boundary, lower_boundary);
        std::string str("Sequential 3D Stencil Run " + std::to_string(i));

        ChronoTimer timer(str);

        stencil3D.calculate_stencil(epsilon_value);

        long time = timer.getElapsedTime();
        times.push_back(time);
    }

    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}