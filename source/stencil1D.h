#ifndef EXERCISE_07_STENCIL1D_H
#define EXERCISE_07_STENCIL1D_H

#include <iostream>
#include <vector>
#include <cmath>


class Stencil1D {
    unsigned int array_size;

    double east_boundary;
    double west_boundary;

    using Array = std::vector<double>;
    Array cells;

public:
    Stencil1D(unsigned int size, double east, double west) :
            array_size(size + 2), east_boundary(east), west_boundary(west) {    // +2 for boundaries; operating area is then from 1 to N

        cells = Array(array_size);

        // setup boundaries
        set_boundaries();

        // std::cout << "Created a Stencil 1D of size " << array_size << " (operating size: " << array_size-2 << ")\n" << std::endl;
    }

    ~Stencil1D() = default;

    void set_boundaries() {
        cells[0] = west_boundary;
        cells[array_size - 1] = east_boundary;
    }

    double jacobi_iteration_1d(const std::vector<double> &in, std::vector<double> &out, size_t size) {
        double sum_changes = 0;

        #pragma omp parallel for reduction(+:sum_changes)
        for(unsigned i = 1; i < size - 1; i++) {
            out[i] = (in[i  ] +         // current position
                      in[i-1] +         // position before current position
                      in[i+1]) / 3;     // position after current position
            sum_changes += std::abs(in[i] - out[i]);
        }

        return sum_changes;
    }

    void calculate_stencil(double epsilon_value) {
        double sum_changes = epsilon_value;

        int iterations = 0;
        Array temp = cells;
        while(sum_changes >= epsilon_value) {   // simulate as long as sum changes is >= 10.0
            jacobi_iteration_1d(temp, cells, array_size);
            sum_changes=jacobi_iteration_1d(cells, temp, array_size);
            iterations += 2;
        }

        // std::cout << "Simulation took " << iterations << " iteration(s)." << std::endl;
    }

    const Array &getCells() const {
        return cells;
    }

    void print_array() {
        for(unsigned i = 0; i < array_size; i++) {
            std::cout << cells[i] << " ";
        }

        std::cout << std::endl;
    }
};

#endif //EXERCISE_07_STENCIL1D_H
