#ifndef EXERCISE07_UTIL_H
#define EXERCISE07_UTIL_H

#include <iostream>
#include <vector>
#include <algorithm>


long median(std::vector<long> times) {
    std::sort(times.begin(), times.end());

    int size = times.size();

    if(size % 2 == 0) { // if the size is even, build the average of the middle two.
        long val1 = times[size / 2 - 1];
        long val2 = times[size / 2];

        return (val1 + val2) / 2l;
    }
    else {
        return times[size / 2];
    }
}

#endif //EXERCISE07_UTIL_H
