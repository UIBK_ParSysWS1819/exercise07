#include <iostream>
#include "stencil1D.h"
#include "chrono_timer.h"
#include "util.h"


int main(int argc, char** argv) {
    // default values
    unsigned int size = 512;
    double east_boundary = 0.5;
    double west_boundary = -0.5;
    double epsilon_value = 0.01;

    if(argc == 5) {
        size = (unsigned) std::stoi(argv[1]);
        east_boundary = std::stod(argv[2]);
        west_boundary = std::stod(argv[3]);
        epsilon_value = std::stod(argv[4]);
    }
    else {
        std::cout << "No input! Default values will be used! "
                     "Usage: ./stencil1D "
                                "<size> "
                                "<east-boundary> "
                                "<west-boundary> " 
                                "<epsilon-value>" << std::endl;
    }

    /*
    std::cout << "size: " << size <<
        "\neast boundary: " << east_boundary <<
        "\nwest boundary: " << west_boundary << 
        "\nepsilon value: " << epsilon_value << std::endl;
    */

    int runs = 7;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        Stencil1D stencil1D(size, east_boundary, west_boundary);
        std::string str("Sequential 1D Stencil Run " + std::to_string(i));

        ChronoTimer timer(str);

        stencil1D.calculate_stencil(epsilon_value);

        long time = timer.getElapsedTime();
        times.push_back(time);
    }

    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}