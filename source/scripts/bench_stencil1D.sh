#!/bin/bash


printf "|Size|Seq|1T|2T|4T|8T|\n"
printf "|---:|---:|---:|---:|---:|---:|\n"

for size in 10000 50000 100000
do
    printf "|%s|" $size

    out_seq=`../stencil1D_seq $size 1.0 -1.0 0.01`
    printf "%s ms|" $out_seq

    for threads in 1 2 4 8
    do
        export OMP_NUM_THREADS=$threads
        out_par=`../stencil1D_omp $size 1.0 -1.0 0.01`
        printf "%s ms|" $out_par
    done
    printf "\n"

done
