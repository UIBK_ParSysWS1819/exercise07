#!/bin/bash

#$ -N stencil_2D

#$ -q std.q

#$ -cwd

#$ -o output_stencil_2D.txt

#$ -j yes

#$ -pe openmp 8

./bench_stencil2D.sh
./bench_stencil2D_icc.sh
