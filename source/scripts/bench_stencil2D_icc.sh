#!/bin/bash

module load intel/15.0

printf "|Size|Seq|1T|2T|4T|8T|\n"
printf "|---:|---:|---:|---:|---:|---:|\n"

for size in 256 384 512
do
    printf "|%s|" $size

    out_seq=`../stencil2D_seq_icc $size 1.0 0.5 0.0 -0.5 10.0`
    printf "%s ms|" $out_seq

    for threads in 1 2 4 8
    do
        export OMP_NUM_THREADS=$threads
        out_par=`../stencil2D_omp_icc $size 1.0 0.5 0.0 -0.5 10.0`
        printf "%s ms|" $out_par
    done
    printf "\n"

done

module unload intel/15.0