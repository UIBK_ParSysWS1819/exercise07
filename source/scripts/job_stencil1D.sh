#!/bin/bash

#$ -N stencil_1D

#$ -q std.q

#$ -cwd

#$ -o output_stencil_1D.txt

#$ -j yes

#$ -pe openmp 8

./bench_stencil1D.sh
./bench_stencil1D_icc.sh
