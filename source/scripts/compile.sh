#!/bin/bash

echo 'compiling gcc'

module load gcc/8.2.0
g++ -std=c++11 -O3 -march=native ../main1D.cpp -o ../stencil1D_seq
g++ -std=c++11 -O3 -march=native -fopenmp ../main1D.cpp -o ../stencil1D_omp

g++ -std=c++11 -O3 -march=native ../main2D.cpp -o ../stencil2D_seq
g++ -std=c++11 -O3 -march=native -fopenmp ../main2D.cpp -o ../stencil2D_omp

g++ -std=c++11 -O3 -march=native ../main3D.cpp -o ../stencil3D_seq
g++ -std=c++11 -O3 -march=native -fopenmp ../main3D.cpp -o ../stencil3D_omp
module unload gcc/8.2.0

echo 'compiling icc'

module load intel/15.0
icpc -std=c++11 -O3 -march=native ../main1D.cpp -o ../stencil1D_seq_icc
icpc -std=c++11 -O3 -march=native -fopenmp ../main1D.cpp -o ../stencil1D_omp_icc

icpc -std=c++11 -O3 -march=native ../main2D.cpp -o ../stencil2D_seq_icc
icpc -std=c++11 -O3 -march=native -fopenmp ../main2D.cpp -o ../stencil2D_omp_icc

icpc -std=c++11 -O3 -march=native ../main3D.cpp -o ../stencil3D_seq_icc
icpc -std=c++11 -O3 -march=native -fopenmp ../main3D.cpp -o ../stencil3D_omp_icc
module unload intel/15.0