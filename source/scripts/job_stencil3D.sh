#!/bin/bash

#$ -N stencil_3D

#$ -q std.q

#$ -cwd

#$ -o output_stencil_3D.txt

#$ -j yes

#$ -pe openmp 8

./bench_stencil3D.sh
./bench_stencil3D_icc.sh
