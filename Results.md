# Stencil

For optimisations we are using only 2 Matrices for storing the previous and the next
iteration. This way we can switch back and forward, read from A save into B
and in the next iteration read from B and save into A. That way we don't need to create
a new Matrix on each iteration. This optimisation made our program two times faster.

For parallelization we simply parallelized the outermost for loop when computing a new iteration,
since we do not have any dependencies in this loop.


## Benchmark results

With the settings as described in the exercise sheet we achieve theses times on the cluster:

gcc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|512|1083 ms|1093 ms|563 ms|295 ms|160 ms|

icc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|512|1121 ms|1244 ms|635 ms|328 ms|242 ms|

We also tried benchmarking with the 1 and 3 dimensional versions, below are some results:

### 1D
epsilon = 0.01

gcc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|10000|227 ms|262 ms|144 ms|119 ms|111 ms|
|50000|641 ms|761 ms|394 ms|244 ms|184 ms|
|100000|1164 ms|1388 ms|705 ms|400 ms|257 ms|

icc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|10000|124 ms|214 ms|119 ms|76 ms|65 ms|
|50000|441 ms|898 ms|454 ms|247 ms|158 ms|
|100000|838 ms|1739 ms|879 ms|459 ms|266 ms|

### 2D
epsilon = 10.0

gcc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|256|59 ms|61 ms|33 ms|18 ms|11 ms|
|384|328 ms|333 ms|174 ms|93 ms|52 ms|
|512|1083 ms|1093 ms|563 ms|295 ms|160 ms|

icc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|256|61 ms|73 ms|38 ms|20 ms|12 ms|
|384|340 ms|386 ms|199 ms|104 ms|57 ms|
|512|1121 ms|1244 ms|635 ms|328 ms|242 ms|

### 3D
epsilon = 100.0

gcc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|50|58 ms|57 ms|30 ms|17 ms|13 ms|
|70|539 ms|528 ms|262 ms|141 ms|89 ms|
|90|2660 ms|2625 ms|1304 ms|719 ms|402 ms|

icc

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|50|73 ms|73 ms|39 ms|22 ms|14 ms|
|70|647 ms|659 ms|329 ms|177 ms|104 ms|
|90|3201 ms|3264 ms|1609 ms|857 ms|466 ms|
