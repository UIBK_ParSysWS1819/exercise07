#include <gtest/gtest.h>
#include "../source/stencil1D.h"
#include "../source/stencil3D.h"
#include "../source/stencil2D.h"


TEST(Stencil_Test, 1D_test) {
    unsigned int size = 10;
    double east_boundary = 0.0;
    double west_boundary = 1.0;
    double epsilon_value = 0.00001;

    double delta = 0.0001;

    Stencil1D stencil1D(size, east_boundary, west_boundary);
    stencil1D.calculate_stencil(epsilon_value);
    
    auto cells = stencil1D.getCells();

    for (int i = 0; i < cells.size(); i++) {
        double analytical_solution = west_boundary - i * std::abs(west_boundary - east_boundary) / ((double) size + 1);
        double numerical_solution = cells[i];
        double error = std::abs(numerical_solution - analytical_solution);

//        std::cout << analytical_solution << ", " << numerical_solution << ", " << error << std::endl;

        EXPECT_TRUE(-delta <= error && error <= delta);
    }
}

TEST(Stencil_Test, 2D_test) {
    unsigned int size = 10;
    double north_boundary = 1.0;
    double east_boundary = 0.0;
    double south_boundary = 0.0;
    double west_boundary = 1.0;
    double epsilon_value = 0.000001;

    double delta = 0.5;

    Stencil2D stencil2D(size, north_boundary, east_boundary, south_boundary, west_boundary);
    stencil2D.calculate_stencil(epsilon_value);

    auto cells = stencil2D.getCells();

    for (int i = 0; i < cells.size(); i++) {
        double analytical_solution = west_boundary - i * std::abs(west_boundary - east_boundary) / ((double) size + 1);

        // for undefined corner case on left lower corner, which should be 1.0
        double numerical_solution = (i == 0) ? west_boundary: cells[i][i];
        double error = std::abs(numerical_solution - analytical_solution);

//        std::cout << analytical_solution << ", " << numerical_solution << ", " << error << std::endl;

        EXPECT_TRUE(-delta <= error && error <= delta);
    }
}

TEST(Stencil_Test, 3D_test) {
    unsigned int size = 10;
    double north_boundary = 0.0;
    double east_boundary = 0.0;
    double south_boundary = 1.0;
    double west_boundary = 1.0;
    double upper_boundary = 0.0;
    double lower_boundary = 1.0;
    double epsilon_value = 0.00001;

    double delta = 0.5;

    Stencil3D stencil3D(size, north_boundary, east_boundary, south_boundary, west_boundary, upper_boundary, lower_boundary);
    stencil3D.calculate_stencil(epsilon_value);
    
    auto cells = stencil3D.getCells();

    for (int i = 0; i < cells.size(); i++) {
        double analytical_solution = west_boundary - i * std::abs(west_boundary - east_boundary) / ((double) size + 1);
        // for undefined corner case on left lower corner, which should be 1.0
        double numerical_solution = (i == 0) ? west_boundary: cells[i][i][i];
        double error = std::abs(numerical_solution - analytical_solution);

//        std::cout << analytical_solution << ", " << numerical_solution << ", " << error << std::endl;

        EXPECT_TRUE(-delta <= error && error <= delta);
    }
}

TEST(Stencil_Test, 1D_fill_test) {
    unsigned int size = 10;
    double east_boundary = 1.0;
    double west_boundary = 1.0;
    double epsilon_value = 0.000001;

    double delta = 0.0001;

    Stencil1D stencil1D(size, east_boundary, west_boundary);
    stencil1D.calculate_stencil(epsilon_value);
    
    auto cells = stencil1D.getCells();
    double analytical_solution = 1.0;

    // leave out boundaries
    for (int i = 1; i < cells.size()-1; i++) {
        double numerical_solution = cells[i];
        double error = std::abs(numerical_solution - analytical_solution);

//        std::cout << analytical_solution << ", " << numerical_solution << ", " << error << std::endl;

        EXPECT_TRUE(-delta <= error && error <= delta);
    }
}

TEST(Stencil_Test, 2D_fill_test) {
    unsigned int size = 10;
    double north_boundary = 1.0;
    double east_boundary = 1.0;
    double south_boundary = 1.0;
    double west_boundary = 1.0;
    double epsilon_value = 0.000001;

    double delta = 0.0001;

    Stencil2D stencil2D(size, north_boundary, east_boundary, south_boundary, west_boundary);
    stencil2D.calculate_stencil(epsilon_value);
    
    auto cells = stencil2D.getCells();
    double analytical_solution = 1.0;

    // leave out boundaries
    for (int i = 1; i < cells.size() -1; i++) {
        double numerical_solution = cells[i][i];
        double error = std::abs(numerical_solution - analytical_solution);
//        std::cout << analytical_solution << ", " << numerical_solution << ", " << error << std::endl;

        EXPECT_TRUE(-delta <= error && error <= delta);
    }
}

TEST(Stencil_Test, 3D_fill_test) {
    unsigned int size = 10;
    double north_boundary = 1.0;
    double east_boundary = 1.0;
    double south_boundary = 1.0;
    double west_boundary = 1.0;
    double upper_boundary = 1.0;
    double lower_boundary = 1.0;
    double epsilon_value = 0.000001;

    double delta = 0.0001;

    Stencil3D stencil3D(size, north_boundary, east_boundary, south_boundary, west_boundary, upper_boundary, lower_boundary);
    stencil3D.calculate_stencil(epsilon_value);
    
    auto cells = stencil3D.getCells();
    double analytical_solution = 1.0;

    // leave out boundaries
    for (int i = 1; i < cells.size()-1; i++) {
        double numerical_solution = cells[i][i][i];
        double error = std::abs(numerical_solution - analytical_solution);

//        std::cout << analytical_solution << ", " << numerical_solution << ", " << error << std::endl;

        EXPECT_TRUE(-delta <= error && error <= delta);
    }
}