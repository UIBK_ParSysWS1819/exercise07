#include "Gridgl.h"
#include "../source/stencil2D.h"

void Gridgl::drawSquare(double x_llower, double y_llower, int side_length, glm::vec3 color) {

    glColor3d(color.r,color.g, color.b);
    glBegin(GL_POLYGON);

//    lu             ru
//
//
//    ll             rl
    glVertex2d(x_llower, y_llower);     //ll
    glVertex2d(x_llower, y_llower + side_length);   //lu
    glVertex2d(x_llower + side_length, y_llower + side_length);     //ru
    glVertex2d(x_llower + side_length, y_llower);   //rl
    glEnd();
}

void Gridgl::display(){
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw Quads
    int quad_size = window_width/grid_size;
    for(int x=0; x<grid_size; x++ ){
        for(int y=0; y<grid_size; y++ ) {
            double h = cells[x+1][y+1];
            std::cout << "Element square " << h <<  " at: " << x*quad_size << " , " << y*quad_size << std::endl;
            h = (1.0 - h) * 240.0;
            glm::vec3 hsvColor{h,1.0,1.0};
            std::cout << "Draw square " << h <<  " at: " << x*quad_size << " , " << y*quad_size << std::endl;
            drawSquare(x*quad_size, y*quad_size, quad_size, glm::rgbColor(hsvColor));
        }
    }

/*
    //Draw wireframes
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    for(int x=0; x<grid_size; x++){
        for(int y=0; y<grid_size; y++) {
            drawSquare(x*quad_size, y*quad_size, quad_size, glm::vec3{0.0, 0.0, 0.0});
        }
    }

    // Turn off wireframe mode
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
*/

    glFlush();
}


void Gridgl::init(){
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // use right upper quad of coordinate system
    gluOrtho2D(0.0, (GLdouble)window_width, 0.0, (GLdouble)window_height);
}

void Gridgl::setCells(const Gridgl::Grid &cells) {
    Gridgl::cells = cells;
}

Gridgl::Gridgl( Gridgl::Grid &cells) : cells(cells) {
    grid_size = cells[0].size()-2;
}

// https://stackoverflow.com/questions/3589422/using-opengl-glutdisplayfunc-within-class

Gridgl* g_CurrentInstance;

extern "C"
void drawCallback(){
    g_CurrentInstance->display();
}

void Gridgl::setupDrawCallback(){
    ::g_CurrentInstance = this;
    ::glutDisplayFunc(::drawCallback);
}

Gridgl::Gridgl(int window_width, int window_height, Gridgl::Grid &cells) : window_width(window_width),
                                                                           window_height(window_height), cells(cells) {
    grid_size = cells[0].size()-2;
}

int main(int argc, char** argv) {
    // default values
    unsigned int size = 10;
    double north_boundary = 1.0;
    double east_boundary = 1.0;
    double south_boundary = 0.0;
    double west_boundary = 0.0;

    if(argc == 6) {
        size = (unsigned) std::stoi(argv[1]);
        north_boundary = std::stod(argv[2]);
        east_boundary = std::stod(argv[3]);
        south_boundary = std::stod(argv[4]);
        west_boundary = std::stod(argv[5]);
    }
    else {
        std::cout << "Warning: No input! Default values will be used! "
                     "Usage: ./stencil2D "
                     "<size> "
                     "<north-boundary> "
                     "<east-boundary> "
                     "<south-boundary> "
                     "<west-boundary>\n" << std::endl;
    }

    std::cout << "size: " << size <<
              "\nnorth boundary: " << north_boundary <<
              "\neast boundary: " << east_boundary <<
              "\nsouth boundary: " << south_boundary <<
              "\nwest boundary: " << west_boundary << "\n" << std::endl;

    double epsilon_value = 0.0001;

    Stencil2D stencil2D(size, north_boundary, east_boundary, south_boundary, west_boundary);



    //stencil2D.print_grid();
    stencil2D.calculate_stencil(epsilon_value);
    Gridgl glgrid{(int)size,(int)size, stencil2D.cells};
    glgrid.render(argc, argv);
    std::cout << "size: " << std::endl;
//    stencil2D.print_grid();

    return EXIT_SUCCESS;
}
