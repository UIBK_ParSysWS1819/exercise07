#ifndef GL_2D_GRID_GLGRID_H
#define GL_2D_GRID_GLGRID_H


#include <vector>
// Include GLM
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/color_space.hpp>

#include <stdio.h>
#include <GL/glut.h>
#include <iostream>



class Gridgl {
private:
    using Grid = std::vector<std::vector<double>>;
    int window_width = 800;
    int window_height = 800;
    unsigned int grid_size;

    static Gridgl* currentInstance;
    void setupDrawCallback();

public:
    Grid& cells;
    Gridgl(Grid &cells);

    Gridgl(int window_width, int window_height, Grid &cells);

    Gridgl();

    void setCells(const Grid &cells);
    void init();

    int render(int argc, char** argv){
        glutInit(&argc, argv);
//    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
        glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
        glutInitWindowSize(window_width, window_height);
        glutInitWindowPosition(100, 150);
        glutCreateWindow("2D-Grid example");
        setupDrawCallback();
//        ::glutDisplayFunc(drawCallback);
        init();
        glutMainLoop();

    }
    void display();
    void drawSquare(double x_center, double y_center, int side_length, glm::vec3 color);
};


#endif //GL_2D_GRID_GLGRID_H
